---
title: BCDC Bharath Cycle Design Challenge
seo:
  title: About
  description: This is the about us page
  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value: About
      keyName: property
    - name: og:description
      value: This is the about us page
      keyName: property
    - name: og:image
      value: images/feature-3.jpg
      keyName: property
      relativeUrl: true
    - name: twitter:card
      value: summary_large_image
    - name: twitter:title
      value: About
    - name: twitter:description
      value: This is the about us page
    - name: twitter:image
      value: images/feature-3.jpg
      relativeUrl: true
layout: page
---

![Outcomes ](http://bcdc.in/images/logo.svg)

Bicycles in India are often overlooked for their invaluable contributions to commute, sports, and wellnes. For this design challenge NNCF is partnering with the many partners  to create a unique set of bicycle designs and build the proto.  The Bharath Cycle Design Challenge "A dedicated recognition of Bicycle in Bharath!" competition tasks its participants with designing a Bicycle for India. The Bharath Cycle Design Challenge  ( BCDC - spoke to Wheel ) marks an important milestone in the roadmap towards establishing the start up hub for e bikes and bicycle to transition India towards Green Mobility.

This highly interactive challenge being conducted in association with premier research institutes, will provide mentorship/bootup  sessions for startups throughout INDIA, including a consideration of how to  MAKE IN INDIA. Private Equity/VC funding can be used to develop new designs  to create the ecosystem for manufacturing cycles/e-bikes in India.

This design challenge  is a set of interactive and facilitated conversations with input from designers  from around the world. We'll surface the most important topics around these themes along with potential obstacles and solutions to addressing those challenges in India by designing solutions around the distributed manufacturing economy. 
 
Participants have total creative freedom and must propose which public environment in which city they feel would benefit most from their markers’ designs.Competition is open to all. No professional qualification is required. Design proposals can be developed individually or by teams (4 team members maximum). Correspondence with organizers must be conducted in English; All information submitted by participants must be in English.

## Inspire Indic Design

BCDC is  linked to the Bharath Context and have the potential to inspire others and enrich the community in which it’s operational. The bicycle would need to be highly versatile and function in any climate and any season. Finding talent on both the design side and the bike craft side- in India - that could work well together; funding; shaping an online competition for a real-life product..
.
## More Power to SMEs

BCDC intends to  harness the power and benefits of the most contemporary tools for manufacturing bicycles. These efforts, along with the kind of work that NNCF is doing through its strategic partnerships, will be critical to Small and Medium Enterprise success in the digital era and beyond in the country
 