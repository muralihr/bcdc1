---
title: Context
sections:
  - type: hero_section
    title: Atmanirbhar Bharath.
    subtitle: Self Reliant Sustainable and Strong India
    content: >-
      **Innovative and Distributed Economy**
    align: center
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/hero-background.jpg
    background_image_opacity: 15
    background_image_size: cover
  - type: grid_section
    grid_items:
      - title: Economics
        title_align: left
        content: >-
          ‘Cycling for short distances can result in an annual benefit of Rs 1.8 trillion to the Indian economy, which is equivalent to 1.6% of India’s annual GDP 
        content_align: left
        image: images/2-days.svg
        image_alt: Section item 1 icon
        image_position: left
        image_width: twenty-five
      - title: Reduce Imports and Increase Exports
        title_align: left
        content: >-
          The total imports of bicycles was more than 7 lakh in 2018–19 and the same is increasing on a year-to-year basis. Of which, more than 50% imports were from China. This apart, India has the potential to export in a big way—beyond USD 360 million exports in 2018–19  ..
        content_align: left
        image: images/2-tracks.svg
        image_alt: Section item 2 icon
        image_position: left
        image_width: twenty-five
      - title: World Leader in Bicycle Manufacturing
        title_align: left
        content: >-
          Explore how Indian Bicycle Industry csn become a world leader in design, engineering and manufacturing and to meet the future demand arising out of the bicycle’s inherent, incredible and indefinite benefits and sustainable character.
        content_align: left
        image: images/20-speakers.svg
        image_alt: Section item 3 icon
        image_position: left
        image_width: twenty-five
    grid_cols: three
    grid_gap_horiz: medium
    grid_gap_vert: medium
    enable_cards: false
    align: center
    background_color: secondary
  - type: features_section
    title: Challenges
    features:
      - content: >-
          India was one of the leaders in Bicycle Manufacturing , but in the last two decades India has LOST the opportunity to be a world leader in exporting bicycles. China and Taiwan have captured the opportunity and have developed complete ecosystems for making the best cycles. BCDC creates multiple windows to explore initiatives to win in the global market. Post Corona with the push for "Make In India" and the reduction on the dependency of chinese imports world over - there is a great opportunity for India to promote cycles and sustainability.
        align: center
    feature_padding_vert: medium
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
  - type: features_section
    features:
      - title: Import from China 
        content: >-
          India Imports around 7-8 lakhs cycles and cycle parts from china. Taiwan a small nation is the leading exporter of cycles to USA. India lost the competetiveness in the 1990’s to china and taiwan - due to strategic policy mistakes and lack of innovation in delivering quality cycles. 
        image: images/feature-china.jpg
        image_alt: Feature 1 placeholder image
        media_position: right
        media_width: sixty
      - title: Lack of Penentration in Indian Middle Class Segment
        content: >-
          Indian cycles not penetrated the middle class market for wellness and attractiveness..The Indian bicycle industry is the second largest in the world, next only to China, with an annual production of 22 million and a turnover of Rs 7000 crore; through 4000 MSMEs that employ nearly one million people in the entire value chain. However, the industry suffers on account of technology gap, lack of usage of superior materials and demand-related bottlenecks. The dependence of the industry on institutional sales is substantial.  Indian Middle class has drifted away from the bicycle to the two wheeler and cars. This shift need to be reversed by enabling cycles that meet the multiple needs of the middle class.  
        image: images/feature-india.jpg
        image_alt: Feature 2 placeholder image
        media_position: left
        media_width: sixty
      - title:  Lack of Bicycle Design Culture
        content: >-
          Indian cycle manufacturing is lagging in research and design to take on the global competition. We just don’t need a designer with a bicycle design. What we need is a design culture for bicycle designing.  This is the first step in this direction , done for the first time in India. Great design starts with the customer.
        image: images/feature-culture.png
        image_alt: Feature 3 placeholder image
        media_position: right
        media_width: sixty
      - title: Design Stagnancy
        content: >-
          Due to the large dependency of the Indian cycle industry on the Institutional Sales, there has been a stagnancy in the make of the cycles as the industry does not see demand in the market for the low cost cycles. With margins very less for these cycles - the design and the make has remained same for the last few decades. 
        image: images/feature-stang.jpg
        image_alt: Feature 1 placeholder image
        media_position: left
        media_width: sixty
    feature_padding_vert: large
    align: center
    padding_top: none
    background_color: none
  - type: cta_section
    title: Make India the World Leader in Manufacturing of Bicycles.
    actions:
      - label: Get tickets
        url: /thank-you
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/cta-background.png
    background_image_opacity: 50
  - type: grid_section
    title: Opportunities 
    subtitle: Vision and Mission to charge the turbo charge thebicyle in India!
    grid_items:
      - title: Premium Bicycle Market
        subtitle: Big Opportunity 
        content: |-
          Revenue forecast in 2028	USD 92.49 billion Globally . Even the premium cycle category seems to be clocking growth. As per industry estimates, the premium bicycle segment (Rs. 15,000 - 30,000) is worth approx. Rs. 350 crore and the super-premium segment (above Rs 30,000) is worth approx. Rs 110 crore in India.  

          **[Slideshare](https://hackmd.io/p/how-to-create-slide-deck#/2)**
        image: images/Global-Bicycle-Market-2.png
        image_alt: Speaker 1 photo
        image_position: top

      - title: Bicycle Parts Market
        subtitle: SME empowerment
        content: |-
          Bicycle Component Market is forecast to reach $9.4 billion by 2025,.

        
          **[Slideshare](https://hackmd.io/p/how-to-create-slide-deck#/2)**
        image: images/market_2.jpg
        image_alt: Speaker 3 photo
        image_position: top
      - title: E-Bikes Market 
        subtitle: easy comfortable bicycles
        content: |-
          Ebike market expected to grow by 200% in another 5 years.

        
          **[Slideshare](https://hackmd.io/p/how-to-create-slide-deck#/2)**
        image: images/ebokmarket.jpg
        image_alt: Speaker 7 photo
        image_position: top
      - title: Bicycle Tourism Market
        subtitle: More demand for cycle tourism market 
        content: |-
          Cycle Tourism is worth 44 billion euros in Europe.

          **[Slideshare](https://hackmd.io/p/template-Talk-slide#/18)**
        image: images/cycling-economy.jpg
        image_alt: Speaker 8 photo
        image_position: top
    grid_cols: four
    grid_gap_horiz: medium
    grid_gap_vert: large
    align: center
    background_color: none

   
   
   
  
  - type: cta_section
    title: Together we can make sure that Bharath thrives in sustainable and innovative mobility.
    
    actions:
      - label: Register
        url: /thank-you
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/cta-background.png
    background_image_opacity: 50
  - type: features_section
    title: Watch the making of the cycles
    subtitle: >-
      2020
    features:
      - title: Meet Phoenix, the Chinese bike company aiming to be industry champions
        content: >-
          Meet Phoenix, the Chinese bike company aiming to be industry champions
        video_embed_html: <iframe width="560" height="315" src="https://www.youtube.com/embed/sSxkwrAM-18" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        media_position: right
        media_width: sixty
      - title: Chinese Bike Factory - Where Do Our Bicycles Come From? | Free Documentary Shorts
        content: >-
          Chinese Bike Factory - Where Do Our Bicycles Come From? | Free Documentary Shorts
        video_embed_html: <iframe width="560" height="315" src="https://www.youtube.com/embed/HC4hkyNI3X8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        media_position: left
        media_width: sixty
      - title: Where Bike Components Are Made – Inside SRAM's Taiwan Factory
        content: >-
          Where Bike Components Are Made – Inside SRAM's Taiwan Factory
        video_embed_html: <iframe width="560" height="315" src="https://www.youtube.com/embed/HGTUCeHLxfI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        media_position: right
        media_width: sixty
    feature_padding_vert: large
    align: center
    padding_top: large
    padding_bottom: large
    background_color: secondary

  - type: grid_section
    grid_items:
      - title: NAMMA NIMMA CYCLE FOUNDATION
        content: |-
          35th main .<br/>
          4th cross BTM II STAGE BENGALURU 68 <br/>
          <br/>
          [bcdc2022@nammacycle.in](mailto:bcdc2022@nammacycle.in)<br/>

          **Contact by Email:** Murali
      - title: Follow Us
        content: |-
          [Youtube](https://www.youtube.com/)<br/>
          [Medium](https://medium.com/)<br/>
          [Facebook](https://www.facebook.com/)<br/>
          [Twitter](https://twitter.com/home)<br/>
    grid_cols: two
    grid_gap_horiz: medium
    grid_gap_vert: large
    enable_cards: false
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
seo:
  title: BCDC
  description:   Bharath Cycle Design Challenge Context


  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value: BCDC
      keyName: property
    - name: og:description
      value: The preview of the Event theme
      keyName: property
    - name: og:image
      value: images/feature-3.jpg
      keyName: property
      relativeUrl: true
    - name: twitter:card
      value: summary_large_image
    - name: twitter:title
      value: BCDC
    - name: twitter:description
      value: The preview of the Event theme
    - name: twitter:image
      value: images/feature-3.jpg
      relativeUrl: true
layout: advanced
---