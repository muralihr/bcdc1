---
title: Home
sections:
  - type: hero_section
    title: Inspire. Create. Build. Make in India.
    subtitle: Bharath Cycle Design Challenge
    content: >-
      **May 25 - 30, 2022**
    actions:
      - label: Register Today!
        url: /about
        style: primary
    align: center
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/hero-background.jpg
    background_image_opacity: 15
    background_image_size: cover
  - type: grid_section
    grid_items:
      - title: Hack + Conf 
        title_align: left
        content: >-
          The Program is designed in 2 stages Stage 1. In the first stage we conduct the Hackathon .Stage 2. Host a Designer and Maker Summit
        content_align: left
        image: images/2-days.svg
        image_alt: Section item 1 icon
        image_position: left
        image_width: twenty-five
      - title: Our  Mission 
        title_align: left
        content: >-
          BCDC envisions  to transform the nicycle design and making stack leading to sustainable mobility. BCDC is the response of india for a sustaining and green economy - where we intend to bring in the innovation in design and modern practices in manufacturability.
        content_align: left
        image: images/2-tracks.svg
        image_alt: Section item 2 icon
        image_position: left
        image_width: twenty-five
      - title: Craft +  Collaboration = Bicycle Design Community - 
        title_align: left
        content: >-
          BCDC is a growing community of Bicycle Designers and Product creators working with tech, across diverse industries, schools and non-profits.  We aim to get the best bicycle designers on our platform and host a virtual design summit . Join us
        actions:
        - label: Join  Today!
          url: /about
          style: primary
        content_align: left
        image: images/20-speakers.svg
        image_alt: Section item 3 icon
        image_position: left
        image_width: twenty-five
    grid_cols: three
    grid_gap_horiz: medium
    grid_gap_vert: medium
    enable_cards: false
    align: center
    background_color: secondary
  - type: features_section
    title:  Hackathon for Bicycle Design in India
    features:
      - content: >-
          The BCDC  Challenge is a national design award that celebrates, encourages and inspires the next generation of designers to rethink the design and making of bicycles in India. It's open to current students/bicycle enthusiasts and is initiated by Namma Nimma Cycle Foundation, as part of its mission to involve young designers in  creating bicycles for people and the planet. That is why we invite design students to explore outdated manufacturing and design processes, and think about what they would do to improve them. By taking an inclusive approach to bicycle design, we can develop exciting new products that contribute value to people, the planet, and the public in India.
        align: center
    feature_padding_vert: medium
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
  - type: features_section
    features:
      - title: Design for India 
        content: >-
           We hope this design challenge may lead to new, disruptive ideas, increasing the chance for India to be the leader in bicycle industry: providing the right bicycle to the people who need it, at the right time, in a convenient, affordable, easy and accessible way, and – if possible – at the lowest cost. We encourage the next generation of designers to use their creative power to shape mobility and transport in india.Create well-designed, practical bicycles that improve access, comfort, safety and convenience  .
        image: images/feature-1.jpg
        image_alt: Feature 1 placeholder image
        media_position: right
        media_width: sixty
      - title: Designers and Design Network for Bicycle Design
        content: >-
          Work in a multidisciplinary team , Learn new design methodologies and skills, Encourage a new generation of designers to become knowledgeable about issues associated with designing-building-making cycles.
        image: images/feature-2.jpg
        image_alt: Feature 2 placeholder image
        media_position: left
        media_width: sixty
      - title: Make in India
        content: >-
          Work on design with impact for transforming the manufacturability of bicycles in India. Dive into a real-life user case - Provide promising designers with a path to drive change in India..
        image: images/feature-3.jpg
        image_alt: Feature 3 placeholder image
        media_position: right
        media_width: sixty
    feature_padding_vert: large
    align: center
    padding_top: none
    background_color: none
  - type: cta_section
    title: Reimagining the bicycle as a vehicle for commons. 
    actions:
      - label: Get tickets
        url: /thank-you
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/cta-background.png
    background_image_opacity: 50
  - type: grid_section
    title: People 
    subtitle: Vision and Mission to charge the turbo charge thebicyle in India! 
    grid_items:
      - title: Murali HR
        subtitle: React Awesome, USA
        content: |-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque interdum ante. Duis tincidunt id purus sit amet malesuada.

          **[Linkedin](https://twitter.com/)**
        image: images/murali.jpg
        image_alt: Speaker 1 photo
        image_position: top
      - title: Nanjunda palacanda
        subtitle: The Studio, Distributed, Estonia
        content: |-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque interdum ante. Duis tincidunt id purus sit amet malesuada.

        
          **[Linkedin](https://twitter.com/)**
        image: images/nanju.jpg
        image_alt: Speaker 3 photo
        image_position: top
      - title: Manoj 
        subtitle: Libris, USA
        content: |-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque interdum ante. Duis tincidunt id purus sit amet malesuada.

        
          **[Linkedin](https://twitter.com/)**
        image: images/kp.jpg
        image_alt: Speaker 7 photo
        image_position: top
      - title: Anup Pai
        subtitle: ReactEvent, Israel
        content: |-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque interdum ante. Duis tincidunt id purus sit amet malesuada.

          **[Linkedin](https://twitter.com/)**
        image: images/ap.jpg
        image_alt: Speaker 8 photo
        image_position: top
    grid_cols: four
    grid_gap_horiz: medium
    grid_gap_vert: large
    align: center
    background_color: none

  - type: grid_section
    title: What People are Saying
    subtitle: Previous event attendees
    grid_items:
      - content: |-
          Larger trend seeking to develop academic and innovative programs that engage with the needs of Atmanirbhar Bharath. Will transform educational institution to be a global, renowned academic institution fostering excellence in design, innovation and entrepreneurship for business, government and society.” 

          **Murali ,** *SID, IISc*
        image: images/muralisid.jpg
        image_alt: Hanson Deck
        image_position: left
        image_width: twenty-five
      - content: |-
          By organizing this competition we remain socially and contextually relevant to bharath by directing our focus to design challenges relevant to “emerging” economies like India.   there is an emerging need for specialised cycles .

          **Prashanth Prakash ,** *Chairman Start Up Cell Karnataka*
        image: images/prashanthaccel.jpg
        image_alt: Miles Tone
        image_position: left
        image_width: twenty-five
      - content: |-
          Been super impressed with concept. Great idea, Great Initiative realy enjoy it!.

          **shamala,** *cycle mechanic, indias first woman cycle designer*
        image: images/shamala.jpg
        image_alt: Eleanor Carr
        image_position: left
        image_width: twenty-five
    grid_cols: six
    grid_gap_horiz: medium
    grid_gap_vert: large
    align: center
    background_color: none
  - type: grid_section
    title: Bicycle Design Themes
    subtitle: BCDC 2022, has multiple themese. BCDC hopes that more bicycles can be created with intelligent technology, sustainable ideas, and sensible design to provide people another option or style of transportation and living. If you have any fantastic ideas, do not hesitate to participate in this competition and share your ideas with everyone.

    grid_items:
      - title: Bicycle for Public Sharing
        subtitle: Around the world, cycle share programs are offering new transportation choices for people of all incomes.
        title_align: left
        content: |-
          ---
          ###   Needs

           They extend the reach of existing transit systems, make one-way cycle trips possible, and eliminate some of the barriers to riding such as cycle ownership, access to storage space, maintenance costs, and concerns about theft.

          * optimal bicycle design for both male and female genders
          * also cover various demands of the public
          * Robust and Maintenence free
        content_align: left
        actions:
          - label: Registration
            url: /thank-you
            style: primary
        actions_align: left
        actions_width: full-width
      - title: E Bike for Indian Women
        subtitle:  Women’s Specific Design gives female riders a better chance at finding a bike that would fit right from the start.
        title_align: left
        content: |-
          ---
          ### Needs

          Many of the design elements  for women’s  bikes, like lower standover height and smaller sizes have to be taken into consideration. 

          * Optimal bicycle design for specifically from the rural and the tier 2 woman requirements would be designed.
          * In the Indian Market there are very few bicycles for women to rely on.
          * Comfort, Easy to ride, Support Indian Women dressing habits
        content_align: left
        actions:
          - label: Registration
            url: /thank-you
            style: primary
        actions_align: left
        actions_width: full-width
      - title: Cargo Bike for Rural India
        subtitle:  develop a low-cost utility/cargo bicycle with rural Indians as the target user. 
        title_align: left
        content: |-
          ---
          ### Needs

          Villagers in India come from various background.

          * convince a manufacturer to produce and distribute this bicycle design to developing countries
          * Robust design
          * Maintenence free for rugged condition
        content_align: left
        actions:
          - label: Registration
            url: /thank-you
            style: primary
        actions_align: left
        actions_width: full-width
      - title: Bicycle Rickshaw or Pedicab
        subtitle: The  Bicycle Rickshaw is one of the most frequently used Taxi in many of the towns in India  
        title_align: left
        content: |-
          ---
          ### Needs

           transforming the lives of their families as well
          
          * to design a lightweight - easy pedallable rickshaw 
          * an option to add power from the passengers also if possible
        content_align: left
        actions:
          - label: Registration
            url: /thank-you
            style: primary
        actions_align: left
        actions_width: full-width
      - title:  Bicycle for disabled
        subtitle: India has significant people who are given bicycles by many non profits.
        title_align: left
        content: |-
          ---
          ### needs

           transforming the lives of their families as well

          * hand pedal with breaks
          * to design a lightweight - easy pedallable bicycle 
          * electric power for disabled bikes. 
        content_align: left
        actions:
          - label: Registration
            url: /thank-you
            style: primary
        actions_align: left
        actions_width: full-width
      - title: Bicycle for Kids
        subtitle: India has the greatest number of kids cycling
        title_align: left
        content: |-
          ---
          ### needs

          Design for Indian Kids

          * Maintenence free  
          * Suggest design for cycles for public distribution
        content_align: left
        actions:
          - label: Register
            url: /thank-you
            style: primary
        actions_align: left
        actions_width: full-width
    grid_cols: three
    grid_gap_horiz: small
    grid_gap_vert: small
    enable_cards: true
    padding_top: small
    padding_bottom: medium
    background_color: primary
    background_image: images/pricing-background.jpg
    background_image_opacity: 10
  - type: grid_section
    title: Our Sponsors
    subtitle: >-
      We Are Supported by Awesome Companies
    align: center
    grid_items:
      - image: images/stackbit.svg
        image_alt: Stackbit logo
        image_align: center
      - image: images/netlify.svg
        image_alt: Netlify logo
        image_align: center
      - image: images/github.svg
        image_alt: GitHub logo
        image_align: center
      - image: images/sticker-mule.svg
        image_alt: Sticker Mule logo
        image_align: center
    grid_cols: four
    grid_gap_horiz: medium
    grid_gap_vert: medium
    padding_top: large
    padding_bottom: large
    background_color: none
  - type: grid_section
    title: Key dates
    subtitle: Deadlines and Targets
    grid_items:
      - title: MAR 15 2022 - ANNOUNCEMENT OF COMPETETION
        title_align: left
        content: >-
         Announcement of the competetion .  You will have until Friday APR 15, 2022 to submit your design proposal. For that we encourage you to do field research, look around the cyclists ,visit cycle shops and  talk with users, and select the BICYLCLE  you would like to design.
        content_align: left
      - title: APR 21, 2022  - ANNOUNCEMENT OF SHORTLISTED ENTRIES 
        title_align: left
        content: >-
          During this phase teams will be shortlisted and notified and you will reassess your design  proposal and challenge it in a fresh, profound way and in the light of your local context. You will start putting more design thinking into the bicycle as a revolutionary vehicle for mobility.
        content_align: left
      - title: MAY 15 to MAY18, 2022 - HACKATHON AT IISC, CPDM BENGALURU.
        title_align: left
        content: >-
          Hackathon is conducted at IISc for which you will be working with your team to produce the best output.
        content_align: left
      - title: JUNE to SEPT, 2022 - PROTO BUILDING FOR MANUFACTURABILITY.
        title_align: left
        content: >-
          The winner and the most promising teams will get the funding to build the bicycle from the themes mentioned above.
        content_align: left
    grid_cols: two
    grid_gap_horiz: medium
    grid_gap_vert: small
    enable_cards: false
    align: center
    padding_top: large
    padding_bottom: large
    background_color: secondary
    background_image: images/faq-background.svg
    background_image_repeat: repeat
    background_image_size: auto
    background_image_opacity: 15
  - type: cta_section
    title: Together we can make sure that Bharath thrives in sustainable and innovative mobility.
    
    actions:
      - label: Register
        url: /thank-you
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/cta-background.png
    background_image_opacity: 50
  - type: grid_section
    grid_items:
      - title: NAMMA NIMMA CYCLE FOUNDATION
        content: |-
          35th main .<br/>
          4th cross BTM II STAGE BENGALURU 68 <br/>
          <br/>
          [bcdc2022@nammacycle.in](mailto:bcdc2022@nammacycle.in)<br/>

          **Contact by Email:** Murali
      - title: Follow Us
        content: |-
          [Youtube](https://www.youtube.com/)<br/>
          [Medium](https://medium.com/)<br/>
          [Facebook](https://www.facebook.com/)<br/>
          [Twitter](https://twitter.com/home)<br/>
    grid_cols: two
    grid_gap_horiz: medium
    grid_gap_vert: large
    enable_cards: false
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
seo:
  title: BCDC
  description: Bharath Cycle Design Challenge


  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value:  BCDC
      keyName: property
    - name: og:description
      value: The preview of the Event theme
      keyName: property
    - name: og:image
      value: images/feature-3.jpg
      keyName: property
      relativeUrl: true
    - name: twitter:card
      value: summary_large_image
    - name: twitter:title
      value: BCDC
    - name: twitter:description
      value: The preview of the Event theme
    - name: twitter:image
      value: images/feature-3.jpg
      relativeUrl: true
layout: advanced
---