---
title: Context
sections:
  - type: hero_section
    title: Support our growing community.
    subtitle:  FOR BCDC, we are committed to innovating and co-creating with a broad partner ecosystem to deliver transformational solutions for Bharath’s unique needs. Our alliances and ecosystem of partners across technology and manufacturing stacks enable us to progress mission-critical technology systems as we work to design, build, manufacture and modernize Bicycle Manufacturing  vital systems. 
    content: >-
      **Why to Partner with us ?**
    align: center
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/hero-background.jpg
    background_image_opacity: 15
    background_image_size: cover
  - type: grid_section
    grid_items:
      - title: Showcase your product.
        title_align: left
        content: >-
          The bicycle design  community and the cycling community are not just curious, they are serious engineering brains.If you are  willing to showcase the product for cycling and wellness community, this would be the place! 
        content_align: left
        image: images/2-days.svg
        image_alt: Section item 1 icon
        image_position: left
        image_width: twenty-five
      - title: Craft , Collaborate and Grow
        title_align: left
        content: >-
           BCDC is a perfect place to connect with students and learners for all levels about bicycle designs: from beginners to those who need an advanced course to seasoned Pros who are looking to switch to a new tool, adopt a new process or discuss the intricacies of bicycle design and the future of mobility.
        content_align: left
        image: images/2-tracks.svg
        image_alt: Section item 2 icon
        image_position: left
        image_width: twenty-five
      - title: Nurture community
        title_align: left
        content: >-
          The bicycle design community in India & SE Asia is in the nascent stage, but learning and growing fast – help them find their voice, help them see the bigger picture, connect the dots. By sharing your knowledge and expertise, sharing tools and processes, through mentoring and speaking you nurture a community of mobility designers and urban designers.
        content_align: left
        image: images/20-speakers.svg
        image_alt: Section item 3 icon
        image_position: left
        image_width: twenty-five
    grid_cols: three
    grid_gap_horiz: medium
    grid_gap_vert: medium
    enable_cards: false
    align: center
    background_color: secondary
  - type: features_section
    title: Interested in supporting the community? 
    features:
      - content: >-
          Sponsor a workshop, or a track. Sponsor the networking dinners or cocktails. Support a challenge or exhibit at one of our many events. Be an education partner or our travel partner. Sponsor scholars or support our content, our videos. There are many meaningful, relevant ways to get your brand to connect to our community. Let's speak .
        align: center
    feature_padding_vert: medium
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
  
  - type: cta_section
    title: Make India the World Leader in Bicycle Design - Support our growing community
    actions:
      - label: Partner  Today
        url: /thank-you
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/cta-bgnew.jpg
    background_image_opacity: 60
  - type: grid_section
    title: Partnership opportunities 
    subtitle:  Exploring the connections between the   fields of mobility, design, and culture.
    grid_items:
      - title: Title Sponsor
        subtitle: React Awesome, USA
        content: |-
          + Media Partner
          + Travel Partner
          + Workshop Partner
          + Presenting Partner
          + Podcast Partner
          + Networking Partner
          **[Linkedin](https://twitter.com/)**
        image: images/title_sponsor.jpg
        image_alt: Speaker 1 photo
        image_position: top
      - title: Gold Sponsor
        subtitle: The Studio, Distributed, Estonia
        content: |-
          + Media Partner
          + Travel Partner
          + Workshop Partner
          + Presenting Partner
          + Podcast Partner
          + Networking Partner
          **[Linkedin](https://twitter.com/)**
        image: images/goldsponsor.jpg
        image_alt: Speaker 3 photo
        image_position: top
      - title: Silver Sponsor
        subtitle: Libris, USA
        content: |-
          + Media Partner
          + Travel Partner
          + Workshop Partner
          + Presenting Partner
          + Podcast Partner
          + Networking Partner 
          **[Linkedin](https://twitter.com/)**
        image: images/silverspo.jpg
        image_alt: Speaker 7 photo
        image_position: top
      - title: Event Partners
        subtitle: Join as a -- 
        content: |-
          + Media Partner
          + Travel Partner
          + Workshop Partner
          + Presenting Partner
          + Podcast Partner
          + Networking Partner -
          **[Linkedin](https://twitter.com/)**
        image: images/eventpartner.png
        image_alt: Speaker 8 photo
        image_position: top
    grid_cols: four
    grid_gap_horiz: medium
    grid_gap_vert: large
    align: center
    background_color: none 
  
  - type: cta_section
    title: BCDC  is always looking for new ventures, opportunities, and connections. If you are interested in working with us, joining us or speaking at one of our events, feel free to reach out to us at murali@nammacycle.in !   
    actions:
      - label: Register
        url: /thank-you
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image:  images/cta-bgnew.jpg
    background_image_opacity: 30
  

  - type: grid_section
    grid_items:
      - title: NAMMA NIMMA CYCLE FOUNDATION
        content: |-
          35th main .<br/>
          4th cross BTM II STAGE BENGALURU 68 <br/>
          <br/>
          [bcdc2022@nammacycle.in](mailto:bcdc2022@nammacycle.in)<br/>
          **Contact by Email:** Murali
      - title: Follow Us
        content: |-
          [Youtube](https://www.youtube.com/)<br/>
          [Medium](https://medium.com/)<br/>
          [Facebook](https://www.facebook.com/)<br/>
          [Twitter](https://twitter.com/home)<br/>
    grid_cols: two
    grid_gap_horiz: medium
    grid_gap_vert: large
    enable_cards: false
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
seo:
  title: BCDC
  description:   Bharath Cycle Design Challenge Context
  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value: BCDC
      keyName: property
    - name: og:description
      value: The preview of the Event theme
      keyName: property
    - name: og:image
      value: images/feature-3.jpg
      keyName: property
      relativeUrl: true
    - name: twitter:card
      value: summary_large_image
    - name: twitter:title
      value: BCDC
    - name: twitter:description
      value: The preview of the Event theme
    - name: twitter:image
      value: images/feature-3.jpg
      relativeUrl: true
layout: advanced
---
